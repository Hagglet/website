package se.experis.SpringBoot.Controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class WebsiteController {

    // Define the endpoint
    @RequestMapping(value = "/help/{resource_name}", method = RequestMethod.GET)
    public String helloSearch(

            @PathVariable String resource_name
    )
    {
        return "Hello " + resource_name;
    }

    // Define the endpoint
    @RequestMapping(value = "/reverse/{resource_name}", method = RequestMethod.GET)
    public StringBuilder reverseSearch(


            @PathVariable String resource_name
    )
    {
        StringBuilder reverseString = new StringBuilder();
        reverseString.append(resource_name);
        return reverseString.reverse();
    }

}